import React from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';

const MyNavbar = ({ bg, variant }) => {
  return (
    <Navbar bg={bg} variant={variant} expand="lg">
      <Container>
        <Navbar.Brand href="/">Mon application</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/about">À propos</Nav.Link>
            <Nav.Link href="/services">Services</Nav.Link>
            <Nav.Link href="/contact">Contact</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default MyNavbar;