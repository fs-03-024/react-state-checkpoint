import React from 'react';
import Navbar from './Navbar';

const Header = ({ navbarBg, navbarVariant }) => {
  return (
    <header>
      <Navbar bg={navbarBg} variant={navbarVariant} />
    </header>
  );
};

export default Header;