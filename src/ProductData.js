import React from 'react';
import ProductItem from './ProductItem';
const ProductData = [
    {
        name: "Smartphone Samsung Galaxy S22",
  image: "https://babi-shop.ci/wp-content/uploads/2022/05/1-4-1.jpg",
  description: "Smartphone haut de gamme avec écran AMOLED, triple caméra et processeur puissant."
};

const product2 = {
  name: "Ordinateur Portable Asus Vivobook",
  image: "https://ci.jumia.is/unsafe/fit-in/500x500/filters:fill(white)/product/58/381872/1.jpg?9062",
  description: "Ordinateur portable fin et léger, avec processeur Intel Core i5 et 8 Go de RAM."
};

const product3 = {
  name: "Enceinte Bluetooth JBL Flip 5",
  image: "https://m.media-amazon.com/images/I/61mQEw77sTS._AC_UF1000,1000_QL80_.jpg",
  description: "Enceinte Bluetooth portable et robuste, offrant un son de qualité et une autonomie de 12 heures."
};