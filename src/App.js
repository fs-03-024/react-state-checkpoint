import logo from './logo.svg';
import './App.css';
import React, { useState } from 'react';
import Header from './components/Navbar/Header';

function App() {
  const [navbarBg, setNavbarBg] = useState('dark');
  const [navbarVariant, setNavbarVariant] = useState('dark');

  const handleNavbarColorChange = (bg, variant) => {
    setNavbarBg(bg);
    setNavbarVariant(variant);
  };

  return (
    <div className="App">
      <Header navbarBg={navbarBg} navbarVariant={navbarVariant} />
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p className='bg-success p-5'>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <div>
          <button
            className="btn btn-primary mr-2"
            onClick={() => handleNavbarColorChange('dark', 'dark')}
          >
            Dark Navbar
          </button>
          <button
            className="btn btn-success"
            onClick={() => handleNavbarColorChange('success', 'light')}
          >
            Light Navbar
          </button>
        </div>
      </header>
    </div>
  );
}

export default App;